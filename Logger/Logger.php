<?php
namespace Vbudnik\RequestPrice\Logger;

class Logger extends \Monolog\Logger {

    public function __construct(array $handlers = [], array $processors = []) {
        parent::__construct('vbudnik_requestprice', $handlers, $processors);
    }

}

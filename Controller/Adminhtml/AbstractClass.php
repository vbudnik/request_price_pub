<?php
namespace Vbudnik\RequestPrice\Controller\Adminhtml;

abstract class AbstractClass extends \Magento\Backend\App\Action {

    protected $_coreRegistry;

    protected $_authSession;

    /**
     * @var \Magento\Backend\Model\View\Result\ForwardFactory
     */
    protected $resultForwardFactory;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Framework\Controller\Result\RawFactory $resultRawFactory,
        \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory,
		\Magento\Backend\Model\Auth\Session $authSession
    ) {
		parent::__construct($context);
        $this->_coreRegistry = $coreRegistry;
        $this->_fileFactory = $fileFactory;
        $this->resultPageFactory = $resultPageFactory;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->resultRawFactory = $resultRawFactory;
        $this->resultForwardFactory = $resultForwardFactory;
        $this->_authSession = $authSession;
    }

    /**
     * @return $this
     */
    protected function _initAction() {
        $resultPage = $this->resultPageFactory->create();
        return $resultPage;
    }

    /**
     * @return bool
     */
    protected function _isAllowed() {
        return $this->_authorization->isAllowed('Vbudnik_RequestPrice::integration');
    }

    /**
     * Init page
     *
     * @param \Magento\Backend\Model\View\Result\Page $resultPage
     * @return \Magento\Backend\Model\View\Result\Page
     */
    protected function initPage($resultPage) {
        $resultPage->setActiveMenu('Vbudnik_RequestPrice::integration_price')
            ->addBreadcrumb(__('Request Price'), __('Request Price'));

        return $resultPage;
    }

    protected function getCurrentAdminId() {
		return $this->_authSession->getUser()->getId();
	}

}

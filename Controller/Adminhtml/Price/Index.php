<?php
namespace Vbudnik\RequestPrice\Controller\Adminhtml\Price;

class Index extends \Vbudnik\RequestPrice\Controller\Adminhtml\AbstractClass {

    public function execute() {
        $resultPage = $this->_initAction();
        $resultPage->getConfig()->getTitle()->prepend(__('Price Request'));
        return $resultPage;
    }

}

<?php
namespace Vbudnik\RequestPrice\Controller\Adminhtml\Price;

class Delete extends \Vbudnik\RequestPrice\Controller\Adminhtml\AbstractClass {

    public function execute() {
		try {
            $id     = (int) $this->getRequest()->getParam('id');
            $model  = $this->_objectManager->create('Vbudnik\RequestPrice\Model\RequestPrice');

            if($id) {
                $model->load($id);
            }

            if(!$model->getId()) {
                throw new \Exception(__('This Price Request no longer exists.'));
            }

            $model->delete();
            $this->messageManager->addSuccess(__('The Price Request has been deleted'));
		} catch(\Exception $e) {
			$this->messageManager->addError(__('An error has occurred: %1', $e->getMessage()));
		}

        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath('*/price/index');
        return $resultRedirect;
    }

}

<?php
namespace Vbudnik\RequestPrice\Controller\Adminhtml\Price;

class Save extends \Vbudnik\RequestPrice\Controller\Adminhtml\AbstractClass {

    public function execute() {
		try {
            $data			=	$this->getRequest()->getPost();

            $id				=	isset($data['id']) ? (int) $data['id'] : null;
            $status			=	isset($data['status']) ? $data['status'] : null;
            $name			=	isset($data['name']) ? $data['name'] : null;
            $email			=	isset($data['email']) ? $data['email'] : null;
			$productSku		=	isset($data['product_sku']) ? $data['product_sku'] : null;
			$comment		=	isset($data['comment']) ? $data['comment'] : null;

            $model			=	$this->_objectManager->create('Vbudnik\RequestPrice\Model\RequestPrice');

            if($id) {
                $model->load($id);

                if(!$model->getId()) {
                    throw new \Exception(__('This Price Request no longer exists.'));
                }
            }

            $model
                ->setName($name)
                ->setEmail($email)
                ->setProductSku($productSku)
                ->setComment($comment)
				->setStatus($status)
				->setAdminId($this->getCurrentAdminId())
                ->save();

            $this->messageManager->addSuccess(__('The Price Request has been saved'));
		} catch(\Exception $e) {
			$this->messageManager->addError(__('An error has occurred: %1', $e->getMessage()));
		}

        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath('*/price/index');
        return $resultRedirect;
    }

}

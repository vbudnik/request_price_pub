<?php
namespace Vbudnik\RequestPrice\Controller\Adminhtml\Price;

class Get extends \Vbudnik\RequestPrice\Controller\Adminhtml\AbstractClass {

    public function execute() {
		try {
            $id     = (int) $this->getRequest()->getParam('id');
            $model  = $this->_objectManager->create('Vbudnik\RequestPrice\Model\RequestPrice');

            if($id) {
                $model->load($id);
            }

            if(!$model->getId()) {
                throw new \Exception(__('This Price Request no longer exists.'));
            }

            return $this->_fileFactory->create(
                basename($model->getFile()),
                file_get_contents($model->getFile()),
                \Magento\Framework\App\Filesystem\DirectoryList::TMP
            );
		} catch(\Exception $e) {
			$this->messageManager->addError(__('An error has occurred: %1', $e->getMessage()));
		}

        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath('*/price/index');
        return $resultRedirect;
    }

}

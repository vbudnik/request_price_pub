<?php
namespace Vbudnik\RequestPrice\Controller\Adminhtml\Price;

class Edit extends \Vbudnik\RequestPrice\Controller\Adminhtml\AbstractClass {

    public function execute() {
        $id = (int) $this->getRequest()->getParam('id');

        $model = $this->_objectManager->create('Vbudnik\RequestPrice\Model\RequestPrice');

        if($id) {
            $model->load($id);

            if(!$model->getId()) {
                $this->messageManager->addError(__('This Price Request no longer exists.'));
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
        }

        $this->_coreRegistry->register('requestprice_price_model', $model);

        $resultPage = $this->_initAction();

        $resultPage->addBreadcrumb(__('Edit Price Request'), __('Edit Price Request'));
        $resultPage->getConfig()->getTitle()->prepend(__('Price Request'));
        $resultPage->getConfig()->getTitle()->prepend(__('Edit Price Request'));

        return $resultPage;
    }

}

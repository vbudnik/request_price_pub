<?php
namespace Vbudnik\RequestPrice\Controller\Adminhtml\Price;

class NewAction extends \Vbudnik\RequestPrice\Controller\Adminhtml\AbstractClass {

    /**
     * Create new Order source
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute() {
        /** @var \Magento\Framework\Controller\Result\Forward $resultForward */
        $resultForward = $this->resultForwardFactory->create();
        return $resultForward->forward('edit');
    }

}

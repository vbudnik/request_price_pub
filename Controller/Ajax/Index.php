<?php

namespace Vbudnik\RequestPrice\Controller\Ajax;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\Result\RedirectFactory;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Controller\ResultFactory;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Customer\Model\Session;

class Index extends Action
{
	const NEW_STATUS = 'new';

	protected $_redirectFactory;
	protected $_jsonFactory;
	protected $_customer;
	protected $_storeManager;
	protected $_customerSession;

	public function __construct(
		Context $context,
		PageFactory $resultPageFactory,
		RedirectFactory $redirectFactory,
		JsonFactory $jsonFactory,
		CustomerRepositoryInterface $customer,
		StoreManagerInterface $storeManager,
		Session $session
	)
	{
		$this->_customerSession = $session;
		$this->_storeManager = $storeManager;
		$this->_customer = $customer;
		$this->_jsonFactory = $jsonFactory;
		$this->_redirectFactory = $redirectFactory->create();
		parent::__construct($context, $resultPageFactory);
	}

	public function execute()
	{
		try {
			$request = [];
			$data			=	$this->getRequest()->getPost();

			$name			=	isset($data['name']) ? $data['name'] : null;
			$email			=	isset($data['email']) ? $data['email'] : null;
			$productSku		=	isset($data['product_sku']) ? $data['product_sku'] : null;
			$comment		=	isset($data['comment']) ? $data['comment'] : null;

			if ($name === null || $email === null || $productSku === null) {
				$request = ['errors' => true];
				$resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
				$resultJson->setData($request);
				return $resultJson;
			}

			$model			=	$this->_objectManager->create('Vbudnik\RequestPrice\Model\RequestPrice');

			$model
				->setName($name)
				->setEmail($email)
				->setProductSku($productSku)
				->setComment($comment)
				->setStatus(\Vbudnik\RequestPrice\Model\RequestPrice::NEW_STATUS)
				->save();

			$request = ['errors' => false];
		} catch(\Exception $e) {
			$request = ['errors' => true];
		}
		$resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
		$resultJson->setData($request);
		return $resultJson;
	}

}

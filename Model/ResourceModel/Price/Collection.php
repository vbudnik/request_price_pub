<?php
namespace Vbudnik\RequestPrice\Model\ResourceModel\Price;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection {

    /**
     * @var string
     */

	protected $_idFieldName = 'id';
	protected $_eventPrefix = 'vbudnik_requestprice_price_collection';
	protected $_eventObject = 'requestprice_price_collection';

//	protected function _construct()
//	{
//		$this->_init('Vbudnik\RequestPrice\Model\RequestPrice', 'Vbudnik\RequestPrice\Model\ResourceModel\RequestPrice');
//	}


	public function __construct(
        \Magento\Framework\Data\Collection\EntityFactoryInterface $entityFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\DB\Adapter\AdapterInterface $connection = null,
        \Magento\Framework\Model\ResourceModel\Db\AbstractDb $resource = null
    ) {
        $this->_init(
            'Vbudnik\RequestPrice\Model\RequestPrice',
            'Vbudnik\RequestPrice\Model\ResourceModel\RequestPrice'
        );

        parent::__construct($entityFactory, $logger, $fetchStrategy, $eventManager, $connection, $resource);

        $this->storeManager = $storeManager;
    }

}

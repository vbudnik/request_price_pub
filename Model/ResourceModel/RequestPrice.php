<?php
namespace Vbudnik\RequestPrice\Model\ResourceModel;

class RequestPrice extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb {

	const MAIN_TABLE = 'vbudnik_requestprice_price';

	public function __construct(
		\Magento\Framework\Model\ResourceModel\Db\Context $context
	)
	{
		parent::__construct($context);
	}


	protected function _construct() {
        $this->_init(self::MAIN_TABLE, 'id');
    }

}

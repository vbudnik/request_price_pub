<?php
namespace Vbudnik\RequestPrice\Model;

class RequestPrice extends \Magento\Framework\Model\AbstractModel {

	const NEW_STATUS = 'new';
	const IN_PROGRESS_STATUS = 'in_progress';
	const CLOSET_STATUS = 'closed';

    /**
     * @var Magento\Framework\Filesystem\Driver\File
     */
    private $filesystem;

    /**
     * @var Magento\Framework\App\Filesystem\DirectoryList
     */
    private $directoryList;

    public function __construct(
        \Magento\Framework\Filesystem\Driver\File $filesystem,
        \Magento\Framework\App\Filesystem\DirectoryList $directoryList,
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
        $this->filesystem = $filesystem;
        $this->directoryList = $directoryList;
    }

    /**
     * Initialize resource model
     * @return void
     */
    protected function _construct() {
        $this->_init('Vbudnik\RequestPrice\Model\ResourceModel\RequestPrice');
    }

	public function getAvailableStatus() {
		return [
			self::NEW_STATUS			=>	__('New'),
			self::IN_PROGRESS_STATUS	=>	__('In Progress'),
			self::CLOSET_STATUS			=>	__('Closed')
		];
	}

}

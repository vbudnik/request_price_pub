<?php
namespace Vbudnik\RequestPrice\Block\Adminhtml\Price;

class Edit extends \Magento\Backend\Block\Widget\Form\Container {

    protected $_coreRegistry;

    /**
     * @var \Magento\Framework\AuthorizationInterface
     */
    private $authorization;

    /**
     * Edit constructor.
     * @param Context $context
     * @param Registry $registry
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\AuthorizationInterface $authorization,
        array $data
    ) {
        $this->_coreRegistry = $coreRegistry;
        $this->authorization = $authorization;
        parent::__construct($context, $data);
    }

    protected function _construct() {
        $this->_blockGroup  = 'Vbudnik_RequestPrice';
        $this->_controller  = 'adminhtml_price';
        $this->_headerText  = __('Request');
        $this->_objectId    = 'vbudnik_requestprice_price';

        $model = $this->_coreRegistry->registry('requestprice_price_model');

        $this->buttonList->update('save', 'label', __('Save Price Request'));
        $this->buttonList->remove('add');

        parent::_construct();
    }

    /**
     * @return mixed
     */
    public function getHeaderText() {
        return __('Edit Price Request');
    }

}

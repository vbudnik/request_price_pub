<?php

namespace Vbudnik\RequestPrice\Block\Adminhtml\Price\Edit;

class Form extends \Magento\Backend\Block\Widget\Form\Generic {

    /**
     * @var
     */
    protected $systemStore;

    /**
     * Form constructor.
     * @param \Magento\Store\Model\System\Store $systemStore
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Store\Model\System\Store $systemStore,
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\Data\FormFactory $formFactory,
        array $data
    ) {
        $this->systemStore = $systemStore;
        parent::__construct($context, $coreRegistry, $formFactory, $data);
    }

    protected function _construct() {
        parent::_construct();
        $this->setId('vbudnik_requestprice_price');
        $this->setTitle(__('Price Information'));
    }

    /**
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _prepareForm() {
        $model = $this->_coreRegistry->registry('requestprice_price_model');

        /* @var FormClass $form */
        $form = $this->_formFactory->create([
            'data' => ['id' => 'edit_form', 'action' => $this->getData('action'), 'method' => 'post', 'enctype' => 'multipart/form-data']
        ]);

        $form->setHtmlIdPrefix('');

        $generalFieldset = $form->addFieldset(
            'general_fieldset',
            ['legend' => __('General Information'), 'class' => 'fieldset-wide']
        );

        $generalFieldset->addField('id', 'hidden', ['name' => 'id']);

        $generalFieldset->addField(
            'name',
            'text',
            [
                'label'     => __('Name'),
                'title'     => __('Name'),
                'name'      => 'name',
                'required'  => true
            ]
        );

        $generalFieldset->addField(
            'email',
            'text',
            [
                'label'     => __('E-mail'),
                'title'     => __('E-mail'),
                'name'      => 'email',
                'required'  => true
            ]
        );

		$generalFieldset->addField(
			'product_sku',
			'text',
			[
				'label'     => __('Product SKU'),
				'title'     => __('Product SKU'),
				'name'      => 'product_sku',
				'required'  => true
			]
		);

		$generalFieldset->addField(
			'comment',
			'text',
			[
				'label'     => __('Comment'),
				'title'     => __('Comment'),
				'name'      => 'comment',
				'required'  => true
			]
		);


        $generalFieldset->addField(
            'status',
            'select',
            [
                'label' => __('Status'),
                'title' => __('Status'),
                'name' => 'status',
                'required' => false,
                'options' => $model->getAvailableStatus()
            ]
        );


        $form->setValues($model->getData());
        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }

}

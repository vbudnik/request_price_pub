<?php
namespace Vbudnik\RequestPrice\Ui\Component\Listing\Column\Price;

class Price extends \Magento\Ui\Component\Listing\Columns\Column {

    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * @param \Magento\Framework\View\Element\UiComponent\ContextInterface $context
     * @param \Magento\Framework\View\Element\UiComponentFactory $uiComponentFactory
     * @param \Magento\Framework\UrlInterface $urlBuilder
     * @param array $components
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\UiComponent\ContextInterface $context,
        \Magento\Framework\View\Element\UiComponentFactory $uiComponentFactory,
        \Magento\Framework\UrlInterface $urlBuilder,
        array $components = [],
        array $data = []
    ) {
        $this->urlBuilder = $urlBuilder;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource) {
        if(isset($dataSource['data']['items'])) {
            foreach($dataSource['data']['items'] as &$item) {
                if(isset($item['id']) && isset($item[$this->getData('name')]) && !empty($item[$this->getData('name')])) {
                    $item[$this->getData('name')] = sprintf('<a href="%s" target="_blank">%s</a>', $this->urlBuilder->getUrl('requestprice/price/get', array('id' => $item['id'])), $item[$this->getData('name')]);
                }
            }
        }

        return $dataSource;
    }

}

<?php
namespace Vbudnik\RequestPrice\Setup;

use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\DB\Ddl\Table;

class UpgradeSchema implements UpgradeSchemaInterface {

    /**
     * Upgrades DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context) {
        $setup->startSetup();

        if(version_compare($context->getVersion(), '1.0.1') < 0) {
            $this->upgradeTo_1_0_1($setup, $context);
        }


        $setup->endSetup();
    }

    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    private function upgradeTo_1_0_1(SchemaSetupInterface $setup, ModuleContextInterface $context)
	{
		$setup->startSetup();
		$tableRequestPrice = $setup->getTable(\Vbudnik\RequestPrice\Model\ResourceModel\RequestPrice::MAIN_TABLE);
		if ($setup->getConnection()->isTableExists($tableRequestPrice) == true) {
			$setup->getConnection()
				->addColumn(
					$setup->getTable($tableRequestPrice),
					'admin_id',
					[
						'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
						'nullable' => true,
						'comment' => 'Admin Id'
					]
				);
		}
        $setup->endSetup();
    }

}
